package com.eltech.artquiz.model.domain;

import java.io.Serializable;

public interface Quiz {

    int getCurrentStep();

    void addStep();

    void addScore();

    boolean isFinished();

    int getTotalScore();

}
