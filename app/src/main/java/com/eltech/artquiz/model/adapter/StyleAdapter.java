package com.eltech.artquiz.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.eltech.artquiz.R;
import com.eltech.artquiz.model.servermodel.Style;
import com.eltech.artquiz.storage.MySharedPreferences;

import java.util.List;

public class StyleAdapter extends BaseAdapter{

    protected List<Style> styles;
    protected Context context;
    protected LayoutInflater layoutInflater;

    public StyleAdapter(List<Style> styles, Context context) {
        this.styles = styles;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return styles.size();
    }

    @Override
    public Object getItem(int i) {
        return styles.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View row = layoutInflater.inflate(R.layout.style_item, viewGroup, false);

        TextView nameTextView = (TextView) row.findViewById(R.id.nameTextView);
        TextView bestScoreTextView = (TextView) row.findViewById(R.id.bestScoreTextView);

        nameTextView.setText(styles.get(i).getName());
        int bestScore = new MySharedPreferences(context).getBestScore(styles.get(i).getId());
        bestScoreTextView.setText(String.valueOf(bestScore));

        return row;
    }
}
