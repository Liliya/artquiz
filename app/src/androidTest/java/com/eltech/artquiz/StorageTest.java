package com.eltech.artquiz;


import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.eltech.artquiz.storage.MySharedPreferences;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class StorageTest {

    private final Context targetContext = InstrumentationRegistry.getTargetContext();
    MySharedPreferences mySharedPreferences;

    @Before
    public void setUp() {
        mySharedPreferences = new MySharedPreferences(targetContext);
    }

    @Test
    public void testSaveBestScore(){
        mySharedPreferences.saveBestScore(1,5);
        Assert.assertEquals(mySharedPreferences.getBestScore(1), 5);
    }

    @Test
    public void testSaveStyleName(){
        mySharedPreferences.saveNameStyle(1,"lalala");
        Assert.assertEquals(mySharedPreferences.getStyleName(1), "lalala");
    }


    @Test
    public void testClean(){
        mySharedPreferences.saveUserId(1);
        mySharedPreferences.saveBestScore(1,5);
        mySharedPreferences.saveUserId(3);
        Assert.assertEquals(mySharedPreferences.getBestScore(1), 0);
    }
}
