package com.eltech.artquiz.storage;

public interface Storage {

    void saveUserId(int userId);

    int getBestScore(int styleId);

    void saveBestScore(int styleId, int score);

    void saveNameStyle(int styleId, String styleName);

    String getStyleName(int styleId);

}
