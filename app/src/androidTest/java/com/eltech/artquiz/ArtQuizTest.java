package com.eltech.artquiz;

import android.support.test.runner.AndroidJUnit4;

import com.eltech.artquiz.model.domain.ArtQuiz;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class ArtQuizTest {

    @Test
    public void testStep() {
        ArtQuiz artQuiz = new ArtQuiz(2);
        artQuiz.addStep();
        artQuiz.addStep();
        Assert.assertEquals(artQuiz.isFinished(), true);
        Assert.assertEquals(artQuiz.getCurrentStep(), 2);
    }

    @Test
    public void testScore() {
        ArtQuiz artQuiz = new ArtQuiz(6);
        Assert.assertEquals(artQuiz.getTotalScore(), 0);
        artQuiz.addScore();
        Assert.assertEquals(artQuiz.getTotalScore(), 1);
    }
}
