package com.eltech.artquiz.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.eltech.artquiz.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultActivity extends AppCompatActivity {

    public final static String IMAGE_PATH_TAG = "IMAGE_PATH";
    public final static String TOTAL_SCORE_TAG = "TOTAL_SCORE";
    public final static String COUNT_TAG = "COUNT_SCORE";

    private int count;
    private int score;
    private String imagePath;
    int styleId;

    @BindView(R.id.scoreTextView)
    TextView scoreTextView;

    @BindView(R.id.artImageView)
    ImageView artImageView;

    @BindView(R.id.personalAccountButton)
    Button personalAccountButton;

    @BindView(R.id.oneMoreTimeButton)
    Button oneMoreTimeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);
        if(getIntent().getExtras()!= null){
            Intent intent = getIntent();
            styleId = intent.getIntExtra(QuizActivity.ID_STYLE, -1);
            imagePath = intent.getStringExtra(IMAGE_PATH_TAG);
            score = intent.getIntExtra(TOTAL_SCORE_TAG, -1);
            count = intent.getIntExtra(COUNT_TAG, -1);
            show();
        }

        oneMoreTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent quizIntent = new Intent(getApplicationContext(), QuizActivity.class);
                quizIntent.putExtra(QuizActivity.ID_STYLE, styleId);
                startActivity(quizIntent);
            }
        });

        personalAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), UserInfoActivity.class));
            }
        });
    }

    private void show(){
        scoreTextView.setText(score  + "/" + count);
        loadImage();
    }

    private void loadImage(){
        if(!imagePath.isEmpty()) {
            Picasso.with(this)
                    .load(imagePath)
                    .into(artImageView);
        } else {
            artImageView.setVisibility(View.GONE);
        }
    }
}
