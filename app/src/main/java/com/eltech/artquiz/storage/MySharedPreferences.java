package com.eltech.artquiz.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.eltech.artquiz.R;

import java.lang.ref.WeakReference;

public class MySharedPreferences implements Storage {

    private static final String USER_ID = "USER_ID";
    private static final String STYLE = "STYLE";
    private static final String STYLE_NAME = "STYLE_NAME";

    WeakReference<SharedPreferences> sharedPrefWeakRef;

    public MySharedPreferences(Context context) {
        sharedPrefWeakRef = new WeakReference<SharedPreferences>(context.getSharedPreferences(
                context.getString(R.string.preference_file_key), Context.MODE_PRIVATE));
    }

    @Override
    public void saveUserId(int userId) {
        if (sharedPrefWeakRef != null) {
            int prevUserId = sharedPrefWeakRef.get().getInt(USER_ID, -1);
            if (prevUserId != userId) {
                clean();
                sharedPrefWeakRef.get()
                        .edit()
                        .putInt(USER_ID, userId)
                        .commit();
            }
        }
    }

    private void clean() {
        if (sharedPrefWeakRef != null) {
            sharedPrefWeakRef.get()
                    .edit()
                    .clear()
                    .commit();
        }
    }

    @Override
    public int getBestScore(int styleId) {
        if (sharedPrefWeakRef != null) {
            return sharedPrefWeakRef.get().getInt(getStyleKey(styleId), 0);
        } else return 0;
    }

    @Override
    public void saveBestScore(int styleId, int score) {
        if (sharedPrefWeakRef != null) {
            if (getBestScore(styleId) < score) {
                sharedPrefWeakRef.get()
                        .edit()
                        .putInt(getStyleKey(styleId), score)
                        .commit();
            }
        }
    }

    @Override
    public void saveNameStyle(int styleId, String styleName) {
        if (sharedPrefWeakRef != null) {
            sharedPrefWeakRef.get()
                    .edit()
                    .putString(STYLE_NAME, styleName)
                    .commit();
        }
    }

    @Override
    public String getStyleName(int styleId) {
        if (sharedPrefWeakRef != null) {
            return sharedPrefWeakRef.get().getString(STYLE_NAME, "");
        } else return "";
    }


    private String getStyleKey(int styleId) {
        return STYLE + styleId;
    }
}
