package com.eltech.artquiz.model.servermodel;

import android.os.Parcel;
import android.os.Parcelable;

public class Answer implements Parcelable {
    public static final Creator<Answer> CREATOR = new Creator<Answer>() {
        @Override
        public Answer createFromParcel(Parcel in) {
            return new Answer(in);
        }

        @Override
        public Answer[] newArray(int size) {
            return new Answer[size];
        }
    };
    Integer id;
    String text;
    Integer quest_id;

    public Answer(Integer id, String text, Integer quest_id) {
        this.id = id;
        this.text = text;
        this.quest_id = quest_id;
    }

    protected Answer(Parcel in) {
        id = in.readInt();
        text = in.readString();
        quest_id = in.readInt();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getQuest_id() {
        return quest_id;
    }

    public void setQuest_id(Integer quest_id) {
        this.quest_id = quest_id;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", quest_id=" + quest_id +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(text);
        parcel.writeInt(quest_id);
    }
}
