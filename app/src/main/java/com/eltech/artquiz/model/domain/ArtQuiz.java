package com.eltech.artquiz.model.domain;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class ArtQuiz implements Quiz, Parcelable {

    private int count;
    private int currentStep = 0;
    private int score = 0;

    public ArtQuiz(int count) {
        this.count = count;
    }

    protected ArtQuiz(Parcel in) {
        count = in.readInt();
        currentStep = in.readInt();
        score = in.readInt();
    }

    public static final Creator<ArtQuiz> CREATOR = new Creator<ArtQuiz>() {
        @Override
        public ArtQuiz createFromParcel(Parcel in) {
            return new ArtQuiz(in);
        }

        @Override
        public ArtQuiz[] newArray(int size) {
            return new ArtQuiz[size];
        }
    };

    @Override
    public int getCurrentStep() {
        return currentStep;
    }

    @Override
    public void addStep() {
        currentStep++;
    }

    @Override
    public int getTotalScore() {
        return score;
    }

    @Override
    public void addScore() {
        score++;
    }

    @Override
    public boolean isFinished() {
        Log.d("restquiz", "currentStep=" + currentStep);
        Log.d("restquiz", "count=" + count);
        return currentStep >= count;
    }

    @Override
    public String toString() {
        return "ArtQuiz{" +
                "count=" + count +
                ", currentStep=" + currentStep +
                ", score=" + score +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(count);
        parcel.writeInt(currentStep);
        parcel.writeInt(score);
    }
}
