package com.eltech.artquiz.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.eltech.artquiz.R;
import com.eltech.artquiz.model.servermodel.Style;
import com.eltech.artquiz.model.adapter.StyleAdapter;
import com.eltech.artquiz.rest.RestResponse;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UserInfoActivity extends AppCompatActivity implements Callback<List<Style>> {
    public static String LIST = "LIST";

    @BindView(R.id.photo)
    ImageView photoImageView;

    @BindView(R.id.nameTextView)
    TextView nameTextView;

    @BindView(R.id.statisticsListView)
    ListView statisticRecyclerListView;

    @BindView(R.id.startQuizButton)
    Button startQuizButton;

    ArrayList<Style> styleList;

    VKApiUser vkApiUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        ButterKnife.bind(this);
        getUserInfo();
        getStyles();
        startQuizButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startQuiz();
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        fillUserInfo();
        StyleAdapter adapter = new StyleAdapter(styleList, getApplicationContext());
        statisticRecyclerListView.setAdapter(adapter);
    }

    private void getStyles() {
        new RestResponse().getStyles().enqueue(this);
    }

    private void getUserInfo() {
        VKRequest user = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "photo_100"));
        user.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                vkApiUser = ((VKList<VKApiUser>) response.parsedModel).get(0);
                fillUserInfo();
            }

            @Override
            public void onError(VKError error) {
                showError();
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                showError();
            }
        });

    }

    private void fillUserInfo() {
        nameTextView.setText(vkApiUser.first_name + " " + vkApiUser.last_name);
        Picasso.with(this).load(vkApiUser.photo_100).into(photoImageView);
    }

    @Override
    public void onResponse(Call<List<Style>> call, Response<List<Style>> response) {
        if (response.isSuccessful()) {
            styleList = new ArrayList<Style>(response.body());
            StyleAdapter adapter = new StyleAdapter(styleList, getApplicationContext());
            statisticRecyclerListView.setAdapter(adapter);
        } else {
            showError();
        }
    }

    @Override
    public void onFailure(Call<List<Style>> call, Throwable t) {
        showError();
    }

    private void showError(){
        Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
        styleList = new ArrayList<>();
        Style one = new Style(1, "Impressionism");
        Style two = new Style(2, "Expressionism");
        styleList.add(one);
        styleList.add(two);
        StyleAdapter adapter = new StyleAdapter(styleList, getApplicationContext());
        statisticRecyclerListView.setAdapter(adapter);
    }

    private void startQuiz(){
        if(styleList!= null){
            Intent quizIntent = new Intent(this, StyleListActivity.class);
            quizIntent.putParcelableArrayListExtra(LIST, styleList);
            startActivity(quizIntent);
        }
    }
}
