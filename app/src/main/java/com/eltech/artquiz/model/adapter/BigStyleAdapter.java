package com.eltech.artquiz.model.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eltech.artquiz.R;
import com.eltech.artquiz.model.servermodel.Style;

import java.util.List;

public class BigStyleAdapter extends StyleAdapter {

    public BigStyleAdapter(List<Style> styles, Context context) {
        super(styles, context);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = layoutInflater.inflate(R.layout.big_style_item, viewGroup, false);
        TextView nameTextView = (TextView) row.findViewById(R.id.nameTextView);
        nameTextView.setText(styles.get(i).getName());
        return row;
    }
}
