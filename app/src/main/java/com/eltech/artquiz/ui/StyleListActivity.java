package com.eltech.artquiz.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.eltech.artquiz.R;
import com.eltech.artquiz.model.adapter.BigStyleAdapter;
import com.eltech.artquiz.model.servermodel.Style;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StyleListActivity extends AppCompatActivity {

    @BindView(R.id.styleListView)
    ListView styleListView;

    ArrayList<Style> styleList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_style_list);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            styleList = getIntent().getParcelableArrayListExtra(UserInfoActivity.LIST);
            styleListView.setAdapter(new BigStyleAdapter(styleList, this));
            styleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    startQuiz(styleList.get(i).getId());
                }
            });
        }
    }

    public void startQuiz(int styleId){
        Intent quizIntent = new Intent(this, QuizActivity.class);
        quizIntent.putExtra(QuizActivity.ID_STYLE, styleId);
        startActivity(quizIntent);
    }
}
