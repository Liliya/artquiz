package com.eltech.artquiz.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.eltech.artquiz.R;
import com.eltech.artquiz.model.adapter.AnswerAdapter;
import com.eltech.artquiz.model.servermodel.Question;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionFragment extends Fragment {

    Question question;

    @BindView(R.id.answersListView)
    ListView answersListView;

    @BindView(R.id.questionTextView)
    TextView questionTextView;

    @BindView(R.id.artImageView)
    ImageView artImageView;

    public static Fragment newInstance(Intent intent) {
        Fragment fragment = new QuestionFragment();
        fragment.setArguments(intent.getExtras());
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fillInfo();
    }

    public void fillInfo() {
        Log.d("question", getQuestion().toString());
        questionTextView.setText(getQuestion().getText());
        loadImage();
        answersListView.setAdapter(new AnswerAdapter(getQuestion().getAnswers(), getActivity()));
        answersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int answerId = getQuestion().getAnswers().get(i).getId();
                boolean rightAnswer = false;
                if (getQuestion().getAnswer_id().equals(answerId)) {
                    rightAnswer = true;
                }
                next(rightAnswer);
            }
        });
    }

    private void loadImage() {
        if (!getQuestion().getImg().isEmpty()) {
            Picasso.with(getActivity())
                    .load(getQuestion().getImg())
                    .into(artImageView);
        } else {
            artImageView.setVisibility(View.GONE);
        }
    }

    private Question getQuestion() {
        if (question == null) {
            question = ((QuizActivity) getActivity()).getCurrentQuestion();
        }
        return question;
    }

    private void next(boolean rightAnswer) {
        Log.d("restquiz", question.toString());
        ((QuizActivity) getActivity()).next(rightAnswer);
    }
}
