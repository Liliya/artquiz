package com.eltech.artquiz.rest;

import com.eltech.artquiz.model.servermodel.Question;
import com.eltech.artquiz.model.servermodel.Style;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestResponse {

    private static final String SITE = "https://look4place.ru";
    ArtApi artAPI;

    public RestResponse() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SITE)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        artAPI = retrofit.create(ArtApi.class);
    }

    public Call<List<Style>> getStyles() {
        return artAPI.getStyles();
    }

    public Call<List<Question>> getQuiz(int count, int styleId) {
        return artAPI.getQuiz(count, styleId);
    }
}
