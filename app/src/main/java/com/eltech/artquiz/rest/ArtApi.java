package com.eltech.artquiz.rest;

import com.eltech.artquiz.model.servermodel.Question;
import com.eltech.artquiz.model.servermodel.Style;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ArtApi {

    @Headers("Content-Type:application/x-www-form-urlencoded; charset=UTF-8")
    @GET("/vapi/styles")
    Call<List<Style>> getStyles();

    @Headers("Content-Type:application/x-www-form-urlencoded; charset=UTF-8")
    @GET("/vapi/question")
    Call<List<Question>> getQuiz(@Query("count") int count, @Query("style") int styleId);
}
