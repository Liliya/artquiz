package com.eltech.artquiz.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.eltech.artquiz.R;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends Activity {

    //fingerprint 7588E4083A7092BB9AAA2B5184FE51F5E970848C

    @BindView(R.id.loginButton)
    Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        autoLogin();
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                goToUserInfo();
            }

            @Override
            public void onError(VKError error) {
                Toast.makeText(getApplicationContext(), getString(R.string.user_didnt_pass_authorization), Toast.LENGTH_SHORT).show();
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void autoLogin() {
//        if (VKSdk.isLoggedIn()) {
//            goToUserInfo();
//        } else {
            login();
//        }
    }

    private void login() {
        VKSdk.login(this);
    }

    private void goToUserInfo() {
        startActivity(new Intent(this, UserInfoActivity.class));
    }
}
