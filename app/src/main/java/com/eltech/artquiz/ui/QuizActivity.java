package com.eltech.artquiz.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.eltech.artquiz.R;
import com.eltech.artquiz.model.domain.ArtQuiz;
import com.eltech.artquiz.model.servermodel.Question;
import com.eltech.artquiz.rest.RestResponse;
import com.eltech.artquiz.storage.MySharedPreferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizActivity extends AppCompatActivity implements Callback<List<Question>> {

    public static String ID_STYLE = "ID_STYLE";
    private static String REST_QUIZ = "REST_QUIZ";
    private static String QUIZ_UTIL = "QUIZ_UTIL";
    private static int COUNT_OF_QUESTIONS = 6;


    private ArtQuiz quizUtil;
    private ArrayList<Question> restQuiz;
    private int styleId = 0;

    QuestionFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        if (savedInstanceState != null) {
            styleId = savedInstanceState.getInt(ID_STYLE);
            quizUtil = savedInstanceState.getParcelable(QUIZ_UTIL);
            Log.d("restquiz", quizUtil.toString());
            restQuiz = savedInstanceState.getParcelableArrayList(REST_QUIZ);
            Log.d("restquiz", restQuiz.toString());
            showQuestion();
        } else if (getIntent().getExtras() != null) {
            styleId = getIntent().getExtras().getInt(ID_STYLE);
            downloadQuiz(styleId);
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        quizUtil = new ArtQuiz(restQuiz.size());
        showQuestion();
    }

    private void downloadQuiz(int styleId) {
        new RestResponse().getQuiz(COUNT_OF_QUESTIONS, styleId).enqueue(this);
    }

    @Override
    public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
        if (response.isSuccessful()) {
            restQuiz = new ArrayList<>(response.body());
            Log.d("restquiz", restQuiz.toString());
            quizUtil = new ArtQuiz(restQuiz.size());
            showQuestion();
        } else {
            showError();
        }
    }

    public void showQuestion() {
        fragment = (QuestionFragment) QuestionFragment.newInstance(getIntent());
        getFragmentManager().beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }

    @Override
    public void onFailure(Call<List<Question>> call, Throwable t) {
        t.printStackTrace();
        showError();
    }

    private void showError() {
        Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
    }

    public Question getCurrentQuestion() {
        Question question = restQuiz.get(quizUtil.getCurrentStep());
        return question;
    }

    public void next(boolean rightAnswer) {
        Log.d("restquiz", String.valueOf(rightAnswer));
        Log.d("restquiz", "score1=" + quizUtil.getTotalScore());
        if (rightAnswer) {
            quizUtil.addScore();
        }
        Log.d("restquiz", "score2=" + quizUtil.getTotalScore());
        quizUtil.addStep();
        if (!quizUtil.isFinished()) {
            showQuestion();
        } else {
            Log.d("restquiz", "finish");
            saveResult();
            startActivityResult(getResultImagePath());
        }
    }

    private String getResultImagePath() {
        int score = quizUtil.getTotalScore();
        if (score >= restQuiz.size()) {
            score = restQuiz.size() - 1;
        }
        return restQuiz.get(score).getImg();
    }

    private void startActivityResult(String imagePath) {
        Intent resultIntent = new Intent(this, ResultActivity.class);
        resultIntent.putExtra(ID_STYLE, styleId);
        resultIntent.putExtra(ResultActivity.IMAGE_PATH_TAG, imagePath);
        resultIntent.putExtra(ResultActivity.TOTAL_SCORE_TAG, quizUtil.getTotalScore());
        resultIntent.putExtra(ResultActivity.COUNT_TAG, restQuiz.size());
        startActivity(resultIntent);
    }

    private void saveResult() {
        MySharedPreferences mySharedPreferences = new MySharedPreferences(getApplicationContext());
        mySharedPreferences.saveBestScore(styleId, quizUtil.getTotalScore());
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        saveInstanceState(outState);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    private void saveInstanceState(Bundle outState){
        outState.putInt(ID_STYLE, styleId);
        outState.putParcelable(QUIZ_UTIL, quizUtil);
        outState.putParcelableArrayList(REST_QUIZ, restQuiz);

    }
}
