package com.eltech.artquiz.model.servermodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Style implements Parcelable{

    private Integer id;
    private String name;

    public static final Creator<Style> CREATOR = new Creator<Style>() {
        @Override
        public Style createFromParcel(Parcel in) {
            return new Style(in);
        }

        @Override
        public Style[] newArray(int size) {
            return new Style[size];
        }
    };

    public Style(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    protected Style(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
    }

}
