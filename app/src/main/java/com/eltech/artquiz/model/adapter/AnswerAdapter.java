package com.eltech.artquiz.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.eltech.artquiz.R;
import com.eltech.artquiz.model.servermodel.Answer;
import com.eltech.artquiz.storage.MySharedPreferences;

import java.util.List;

public class AnswerAdapter extends BaseAdapter{

    List<Answer> answers;
    protected Context context;
    protected LayoutInflater layoutInflater;

    public AnswerAdapter(List<Answer> answers, Context context) {
        this.answers = answers;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return answers.size();
    }

    @Override
    public Object getItem(int i) {
        return answers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = layoutInflater.inflate(R.layout.answer_item, viewGroup, false);
        TextView nameTextView = (TextView) row.findViewById(R.id.nameTextView);
        nameTextView.setText(answers.get(i).getText());
        return row;
    }
}
