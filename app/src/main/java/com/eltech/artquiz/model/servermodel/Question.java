package com.eltech.artquiz.model.servermodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Question implements Parcelable {

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };
    Integer id;
    String text;
    String img;
    List<Answer> answers;
    Integer answer_id;

    public Question(Integer id, String text, String img, List<Answer> answers, Integer answer_id) {
        this.id = id;
        this.text = text;
        this.img = img;
        this.answers = answers;
        this.answer_id = answer_id;
    }

    protected Question(Parcel in) {
        id = in.readInt();
        text = in.readString();
        img = in.readString();
        in.readList(answers, Answer.class.getClassLoader());
        answer_id = in.readInt();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public Integer getAnswer_id() {
        return answer_id;
    }

    public void setAnswer_id(Integer answer_id) {
        this.answer_id = answer_id;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", img='" + img + '\'' +
                ", answers=" + answers +
                ", answer_id=" + answer_id +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(text);
        parcel.writeString(img);
        parcel.writeList(answers);
        parcel.writeInt(answer_id);
    }
}
